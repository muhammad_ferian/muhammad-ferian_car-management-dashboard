require('dotenv').config()
const express = require('express')
const port = process.env.PORT || 3500
const app = express()
const cors = require('cors')
const bodyParser = require('body-parser');
const router = require('./router')
const expressLayouts = require('express-ejs-layouts')
const path = require('path')
const displayRouter = require('./router/display')

app.use(express.static(path.join(__dirname, '/public')));

app.set('view engine', 'ejs')
app.use(expressLayouts)
app.use(cors())
app.use(express.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use('/', router)
app.get('/', displayRouter)

app.listen(port, () => {
  console.log(`Server is running at port ${port}`)
})
