const express = require('express')
const app = express()
const router = express.Router()
const carRouter = require('./car.js')

router.get('/check', (req, res) => res.send("App Up"))
router.use('/api', carRouter)

module.exports = router