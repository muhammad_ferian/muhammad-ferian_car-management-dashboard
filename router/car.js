const express = require('express')
const router = express.Router()
const { list, get, create, update, destroy, filter } = require('../controllers/carController.js')

router.get('/cars', list)
router.get('/car/:id', get)
router.post('/car', create)
router.put('/update/:id', update)
router.delete('/destroy/:id', destroy)
router.get('/filter/:size', filter)

module.exports = router