# FSW Challenge 5

## URL

```http
http://localhost:3000/ => Home Page
```

## Disclaimer

Pada aplikasi ini, saya hanya menyelesaikan pembuatan API untuk CRUD.
Untuk bagian views saya hanya menyelesaikan home page

## ERD

| Column          | Type            |
| --------------- | --------------- |
| id              | integer         |
| name            | varchar(255)    |
| price           | float           |
| size            | varchar(255)    |
| foto            | varchar(255)    |
| createdAt       | timestamp       |
| updatedAt       | timestamp       |

## API

### 1) Get all cars

```http
  GET http://localhost:3000/api/cars
```

#### Response

```javascript
[
    {
        "id": 1,
        "name": "inova",
        "price": 500000,
        "size": "large",
        "foto": "foto.png",
        "createdAt": "2022-04-20T15:56:35.117Z",
        "updatedAt": "2022-04-20T15:56:35.117Z"
    },
    {
        "id": 2,
        "name": "avanza",
        "price": 500000,
        "size": "large",
        "foto": "foto.png",
        "createdAt": "2022-04-20T16:04:47.314Z",
        "updatedAt": "2022-04-21T09:18:22.065Z"
    },
    {
        "id": 7,
        "name": "rush",
        "price": 350000,
        "size": "medium",
        "foto": "foto.png",
        "createdAt": "2022-04-21T16:00:27.798Z",
        "updatedAt": "2022-04-21T16:00:27.798Z"
    }
]
```

### 2) Get car by ID

```http
  GET http://localhost:3000/api/car/:id
```

| Parameter | Type     | Description   |
| :-------- | :------- | :------------ |
| `id`      | `Number` | **Required**  |

#### Response

```javascript
{
    "id": 1,
    "name": "inova",
    "price": 500000,
    "size": "large",
    "foto": "foto.png",
    "createdAt": "2022-04-20T15:56:35.117Z",
    "updatedAt": "2022-04-20T15:56:35.117Z"
}
```

### 3) Insert new car

```http
  POST http://localhost:3000/api/car
```

#### Request Body

| Body    | Type     | Description   |
| :------ | :------- | :------------ |
| `name`  | `String` | **Required**  |
| `price` | `String` | **Required**  |
| `foto`  | `String` | **Required**  |
| `size`  | `String` | **Required**  |

#### Request Body Example

```javascript
{
    "name" : "rush",
    "price" : "350000",
    "size" : "medium",
    "foto" : "foto.png"
}
```

#### Response

```javascript
{
    "id": 7,
    "name": "rush",
    "price": 350000,
    "size": "medium",
    "foto": "foto.png",
    "updatedAt": "2022-04-21T16:00:27.798Z",
    "createdAt": "2022-04-21T16:00:27.798Z"
}
```

### 4) Update car by ID

```http
  PUT http://localhost:3000/api/update/:id
```

#### Request Params

| Parameter | Type     | Description  |
| :-------- | :------- | :----------- |
| `id`      | `Number` | **Required** |

#### Request Body

| Body    | Type     | Description   |
| :------ | :------- | :------------ |
| `name`  | `String` | **Required**  |
| `price` | `String` | **Required**  |
| `image` | `String` | **Required**  |
| `size`  | `String` | **Required**  |

#### Request Body Example

```javascript
{
    "name": "rush",
    "price": 250000,
    "size": "small",
    "foto": "foto.png"
}
```

#### Response

```javascript
{
    "id": 7,
    "name": "rush",
    "price": 250000,
    "size": "small",
    "foto": "foto.png",
    "createdAt": "2022-04-21T16:00:27.798Z",
    "updatedAt": "2022-04-21T17:40:55.426Z"
}
```

### 5) Delete car by ID

```http
  DELETE http://localhost:3000/api/destroy/:id
```

| Parameter | Type     | Description   |
| :-------- | :------- | :------------ |
| `id`      | `Number` | **Required**  |

#### Response

```javascript
data deleted
```

### 6) Filter car by size

```http
  GET http://localhost:3000/api/car/:size
```

| Parameter   | Type     | Description   |
| :--------   | :------- | :------------ |
| `size`      | `String` | **Required**  |

#### Response

```javascript
[
    {
        "id": 1,
        "name": "inova",
        "price": 500000,
        "size": "large",
        "foto": "foto.png",
        "createdAt": "2022-04-20T15:56:35.117Z",
        "updatedAt": "2022-04-20T15:56:35.117Z"
    },
    {
        "id": 2,
        "name": "avanza",
        "price": 500000,
        "size": "large",
        "foto": "foto.png",
        "createdAt": "2022-04-20T16:04:47.314Z",
        "updatedAt": "2022-04-21T09:18:22.065Z"
    }
]
```
