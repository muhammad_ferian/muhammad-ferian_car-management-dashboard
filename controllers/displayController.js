const axios = require("axios");

exports.index = (req, res) => {
  axios
    .get('http://localhost:3000/api/cars')
    .then((resp) => {
      const data = {
        layout: 'layouts/main-layouts',
        cars: resp.data,
      };
      res.render('index', data);
    })
    .catch((error) => {
      console.log(error);
    });
};