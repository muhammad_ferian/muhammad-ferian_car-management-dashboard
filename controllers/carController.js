const { status } = require('express/lib/response');
const model = require('../models');

module.exports = {
  list: async (req, res) => {
    try {
      const datas = await model.CarRent.findAll();

      return res.send(datas)

    } catch (error) {
      return res.status(500).json({
        success: false,
        error: error.code,
        message: error,
        data: null,
      })
    }
  },
  create: async (req, res) => {
    try {
      const data = await model.CarRent.create(req.body);

      return res.send(data)

    } catch (error) {
      return res.status(500).json({
        success: false,
        error: error.code,
        message: error,
        data: null
      })
    }
  },
  update: async (req, res) => {
    try {
      const query = await model.CarRent.update(
        {
            name: req.body.name,
            price: req.body.price,
            size: req.body.size,
            foto: req.body.foto,
        },
        {
          where: {
            id: req.params.id
          },
        }
      )
      const data = await model.CarRent.findOne({
        where: { id: req.params.id }
      })
      return res.send(data)

    } catch (error) {
      return res.status(500).json({
        success: false,
        error: error.code,
        message: error,
        data: null
      })
    }
  },
  destroy: async (req, res) => {
    try {
      const data = await model.CarRent.destroy({
        where: {
          id: req.params.id,
        },
      })
      
      return res.send('data deleted')

    } catch (error) {
      return res.status(500).json({
        success: false,
        error: error.code,
        message: error,
        data: null
      })
    }
  },
  get: async (req, res) => {
    try {
      const data = await model.CarRent.findOne({
        where: {
          id: req.params.id
        }
      })

      return res.send(data)

      } catch (error) {
          return res.status(500).json({
            success: false,
            error: error.code,
            message: error,
            data: null
        });
      }
  },
  filter: async (req, res) => {
    try {
      const datas = await model.CarRent.findAll({
        where: {
          size: req.params.size
        }
      })

      return res.send(datas)

      } catch (error) {
          return res.status(500).json({
            success: false,
            error: error.code,
            message: error,
            data: null
        });
      }
  }
};